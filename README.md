# CI

Overview of my *GitLab CI* configuration setup.

# Runners

Currently runners for *x64* and *ARM64* with *Arch Linux* are running:

| Parameter      | *x64*                   | *ARM64*
| -------------  | ----------------------- | -----------------------
| Runner version | 9.1.0~beta.67.g659562d7 | 9.1.0~beta.71.gf88ddd5e
| Hardware       | Virtual server          | ODROID-C2

**Notice**: Currently, there's no offical pre-built runner available for *ARM64*.
I wrote a detailed blog entry (in German) about compiling it for *ARM64*, see it
[here](https://schweter.eu/2016/10/gitlab-runner-arm64.html).

# Setup

The virtual server has *Arch Linux* installed as host system. *Debian* and
*Ubuntu* runners are executed in containers controlled by `systemd-nspawn`.

# Distributions

The following *GNU/Linux* distributions are enabled for my CI setup and run
in different containers:

| Distribution      | Release                   | Architecture | CI Tags
| ----------------- | ------------------------- | ------------ | -------------------
| *Arch Linux*      | Rolling                   | *ARM64*      | arm64
| *Debian*          | *Jessie*                  | *x64*        | debian, jessie, x64
| *Debian*          | *Sid*                     | *x64*        | debian, sid, x64
| *Ubuntu*          | 17.04 (*Zesty Zapus*)     | *x64*        | ubuntu, zesty, x64
| *Ubuntu*          | 17.10 (*Artful Aardvark*) | *x64*        | ubuntu, artful. x64

I documented the container creation steps and the installed libraries for
[Debian](doc/debian.md) and [Ubuntu](doc/ubuntu.md).

# Issues

An issue tracker can be found [here](https://gitlab.com/stefan-it/ci/issues).
