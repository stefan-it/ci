# *Debian* Installation

On my *Arch Linux* host system, the following *Debian* releases are installed:

* Jessie
* Sid

Therefore, the `debootstrap` package from *Arch Linux* was installed via:

```bash
pacman -Sy debootstrap
```

## *Debian* Jessie container

A *Debian* Jessie container was configured via `debootstrap` with:

```bash
debootstrap --verbose --merged-usr --variant=minbase --include=systemd-sysv --exclude=sysv-rc,initscripts,startpar,lsb-base,insserv jessie /var/lib/machines/debian-jessie
```

After that, `systemd-nspawn` can start the container:

```bash
systemd-nspawn -M debian-jessie
```

After the configuration, the `--boot` option is used to start the container and
its systemd inside.

## *Debian* sid container

A *Debian* Sid container was configured via `debootstrap` with:

```bash
debootstrap --verbose --merged-usr --variant=minbase --include=systemd-sysv --exclude=sysv-rc,initscripts,startpar,lsb-base,insserv sid /var/lib/machines/debian-sid
```

After that, `systemd-nspawn` can start the container:

```bash
systemd-nspawn -M debian-sid
```

After the configuration, the `--boot` option is used to start the container and
its systemd inside.

# Packages

The following dependencies are currently installed for my CI setup:

* `locales`
* `build-essential`
* `clang`
* `libboost-all-dev`
* `cmake`
* `git-core`
* `wget`
* `curl`
* `libmicrohttpd-dev`
* `libjsoncpp-dev`

On *Debian* sid the `experimental` [repository](https://wiki.debian.org/DebianExperimental)
is enabled via:

```bash
echo "deb http://deb.debian.org/debian experimental main" >> /etc/apt/sources.list
```

Then *GCC* 7 is installed:

```bash
apt-get -t experimental install gcc-7 g++-7
```

Furthermore, `clang` in version *5* was installed:

```bash
apt-get install clang-5.0
```

The following symlinks needs to be set:

```bash
ln -sv /usr/bin/clang-5.0 /usr/bin/clang
ln -sv /usr/bin/clang++-5.0 /usr/bin/clang++
```