# *Ubuntu* Installation

On my *Arch Linux* host system, the following *Ubuntu* releases are installed:

* 17.04 (*Zesty Zapus*)
* 17.10 (*Artful Aardvark*)

Therefore, the `debootstrap` package from *Arch Linux* was installed via:

```bash
pacman -Sy debootstrap
```

## *Ubuntu* 17.04 (*Zesty Zapus*) container

A *Ubuntu* 17.04 (*Zesty Zapus*) was configured via `debootstrap` with:

```bash
debootstrap --verbose --merged-usr --variant=minbase --include=systemd-sysv --exclude=sysv-rc,initscripts,startpar,lsb-base,insserv zesty /var/lib/machines/ubuntu-zesty http://de.archive.ubuntu.com/ubuntu
```

After that, `systemd-nspawn` can start the container:

```bash
systemd-nspawn -M ubuntu-zesty
```

After the configuration, the `--boot` option is used to start the container and
its systemd inside.

## *Ubuntu* 17.10 (*Artful Aardvark*) container

*Ubuntu* 17.10 (*Artful Aardvark*) container was configured via `debootstrap`
with:

```bash
debootstrap --verbose --merged-usr --variant=minbase --include=systemd-sysv --exclude=sysv-rc,initscripts,startpar,lsb-base,insserv artful /var/lib/machines/ubuntu-artful http://de.archive.ubuntu.com/ubuntu
```

After that, `systemd-nspawn` can start the container:

```bash
systemd-nspawn -M ubuntu-artful
```

After the configuration, the `--boot` option is used to start the container and
its systemd inside.

# Packages

## Universe repository

For *Ubuntu* 17.04 (*Zesty Zapus*) the universe repository must be added
to `/etc/apt/sources.list`:

```bash
deb http://de.archive.ubuntu.com/ubuntu zesty universe
```

The same must be done for *Ubuntu* 17.10 (*Artful Aardvark*):

```bash
deb http://de.archive.ubuntu.com/ubuntu artful universe
```

## Installation

The following dependencies are currently installed for my CI setup:

* `locales`
* `build-essential`
* `clang-4.0`
* `libboost-all-dev`
* `cmake`
* `git-core`
* `wget`
* `curl`
* `libmicrohttpd-dev`
* `libjsoncpp-dev`

## Fixes

Notice: as there's no `clang` package available, no symlink for `clang`
to `clang-4.0` will be created. This must be done manually for both *Ubuntu*
17.04 (*Zesty Zapus*) and *Ubuntu* 17.10 (*Artful Aardvark*):

```bash
ln -sv /usr/bin/clang-4.0 /usr/bin/clang
ln -sv /usr/bin/clang++-4.0 /usr/bin/clang++
```
